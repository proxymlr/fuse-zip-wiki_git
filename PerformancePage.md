﻿# VFS performance comparison

# Tested VFSs

|VFS       |Version      |Description                                                                                                                                        |
|:---------|:------------|:-------------------------------------------------------------------------------------------------------------------------|
|fuse-zip  |  >=0.2.0    |     FUSE filesystem to mount, extract and modify ZIP                            archives (https://bitbucket.org/agalanin/fuse-zip)|
|kio-zip   |  part of KDE3 |   KDE virtual filesystem                                                                                                 |
|kio-krarc |  part of KDE3 |   Krusader's virtual filesystem                                                                                          |
|mc-uzip   |  part of mc |     Midnight Commander VFS module                                                                                        |
|gfvs-fuse |  >=0.2.4    |     Gnome userspace virtual filesystem - fuse bridge                                                                     |
|unpackfs  |  >=0.0.6    |     Filesystem based on fuse (filesystem in userspace)                            for transparent unpacking of archives                            (http://savannah.nongnu.org/projects/unpackfs/)|
|avfs-fuse |  >=0.9.8    |     AVFS is a system, which enables all programs to look inside archived or compressed files, or                           access remote files without recompiling the                            programs or changing the kernel                            (http://sourceforge.net/projects/avf/)|
|fusej-zip |  2.4-prerelease1 | FUSE-J provides Java binding for FUSE. It comes                            with the "proof-of-concept" ZIP filesystem which                            seems to be pretty stable. (http://sourceforge.net/projects/fuse-j)|


# Test results

## Compress files generated from /dev/zero (zip-zero)
| Program | real | user | system |
|:--------|:-----|:-----|:-------|
| fuse-zip	| 0.65	| 0.01	| 0.04  |
| kio-zip	 | n/a	 | n/a	 | n/a   |
| kio-krarc	| 0.67	| 0.08	| 0.04  |
| mc-uzip	 | 1.05	| 0.92	| 0.12  |
| gvfs-fuse	| n/a	 | n/a	 | n/a   |
| unpackfs	| n/a	 | n/a	 | n/a   |
| avfs-fuse	| n/a	 | n/a	 | n/a   |
| fusej-zip	| n/a	 | n/a	 | n/a   |

## Uncompress files generated from /dev/zero (unzip-zero)
| Program | real | user | system |
|:--------|:-----|:-----|:-------|
| fuse-zip	| 0.20	| 0.00	| 0.03  |
| kio-zip	 | 0.32	| 0.06	| 0.04  |
| kio-krarc	| 0.32	| 0.05	| 0.00  |
| mc-uzip	 | 1.05	| 0.80	| 0.25  |
| gvfs-fuse	| 1.84	| 0.20	| 0.07  |
| unpackfs	| 0.32	| 0.00	| 0.10  |
| avfs-fuse	| 0.23	| 0.00	| 0.02  |
| fusej-zip	| 0.59	| 0.38	| 0.18  |

## Compress files generated from /dev/urandom (zip-urandom)
| Program | real | user | system |
|:--------|:-----|:-----|:-------|
| fuse-zip	| 1.80	| 0.03	| 0.09  |
| kio-zip	 | n/a	 | n/a	 | n/a   |
| kio-krarc	| 2.11	| 0.07	| 0.03  |
| mc-uzip	 | 2.10	| 1.88	| 0.20  |
| gvfs-fuse	| n/a	 | n/a	 | n/a   |
| unpackfs	| n/a	 | n/a	 | n/a   |
| avfs-fuse	| n/a	 | n/a	 | n/a   |
| fusej-zip	| n/a	 | n/a	 | n/a   |

## Uncompress files generated from /dev/urandom (unzip-urandom)
| Program | real | user | system |
|:--------|:-----|:-----|:-------|
| fuse-zip	| 0.22	| 0.00	| 0.08  |
| kio-zip	 | 0.28	| 0.06	| 0.04  |
| kio-krarc	| 0.37	| 0.06	| 0.00  |
| mc-uzip	 | 1.13	| 0.89	| 0.24  |
| gvfs-fuse	| 1.97	| 0.14	| 0.22  |
| unpackfs	| 0.51	| 0.01	| 0.12  |
| avfs-fuse	| 0.26	| 0.00	| 0.07  |
| fusej-zip	| 0.58	| 0.38	| 0.17  |

## Compress files generated from /dev/{urandom,zero} (zip-mixed)
| Program | real | user | system |
|:--------|:-----|:-----|:-------|
| fuse-zip	| 3.30	| 0.04	| 0.20  |
| kio-zip	 | n/a	 | n/a	 | n/a   |
| kio-krarc	| 3.46	| 0.12	| 0.05  |
| mc-uzip	 | 3.14	| 2.84	| 0.29  |
| gvfs-fuse	| n/a	 | n/a	 | n/a   |
| unpackfs	| n/a	 | n/a	 | n/a   |
| avfs-fuse	| n/a	 | n/a	 | n/a   |
| fusej-zip	| n/a	 | n/a	 | n/a   |

## Uncompress files generated from /dev/{urandom,zero} (unzip-mixed)
| Program | real | user | system |
|:--------|:-----|:-----|:-------|
| fuse-zip	| 0.42	| 0.00	| 0.10  |
| kio-zip	 | 0.53	| 0.07	| 0.08  |
| kio-krarc	| 0.65	| 0.06	| 0.01  |
| mc-uzip	 | 2.18	| 1.64	| 0.44  |
| gvfs-fuse	| 2.28	| 0.30	| 0.32  |
| unpackfs	| 0.80	| 0.01	| 0.17  |
| avfs-fuse	| 0.49	| 0.00	| 0.13  |
| fusej-zip	| 0.89	| 0.56	| 0.31  |

## Extract one file from archive with many files (extract-one-1)
| Program | real | user | system |
|:--------|:-----|:-----|:-------|
| fuse-zip	| 0.03	| 0.01	| 0.01  |
| kio-zip	 | 0.09	| 0.04	| 0.01  |
| kio-krarc	| 0.18	| 0.07	| 0.03  |
| mc-uzip	 | 0.08	| 0.07	| 0.01  |
| gvfs-fuse	| 1.61	| 0.05	| 0.04  |
| unpackfs	| 1.33	| 0.01	| 0.02  |
| avfs-fuse	| 0.03	| 0.00	| 0.01  |
| fusej-zip	| 0.29	| 0.20	| 0.07  |

## Extract one file from big archive (extract-one-2)
| Program | real | user | system |
|:--------|:-----|:-----|:-------|
| fuse-zip	| 0.03	| 0.00	| 0.00  |
| kio-zip	 | 0.09	| 0.03	| 0.02  |
| kio-krarc	| 0.18	| 0.09	| 0.02  |
| mc-uzip	 | 0.08	| 0.04	| 0.03  |
| gvfs-fuse	| 1.58	| 0.03	| 0.03  |
| unpackfs	| 0.62	| 0.01	| 0.01  |
| avfs-fuse	| 0.03	| 0.01	| 0.00  |
| fusej-zip	| 0.28	| 0.21	| 0.05  |

## Add small file to small archive (add-small-small)
| Program | real | user | system |
|:--------|:-----|:-----|:-------|
| fuse-zip	| 0.05	| 0.01	| 0.01  |
| kio-zip	 | n/a	 | n/a	 | n/a   |
| kio-krarc	| 0.13	| 0.04	| 0.02  |
| mc-uzip	 | 0.09	| 0.06	| 0.02  |
| gvfs-fuse	| n/a	 | n/a	 | n/a   |
| unpackfs	| n/a	 | n/a	 | n/a   |
| avfs-fuse	| n/a	 | n/a	 | n/a   |
| fusej-zip	| n/a	 | n/a	 | n/a   |

## Add small file to big archive (add-small-big)
| Program | real | user | system |
|:--------|:-----|:-----|:-------|
| fuse-zip	| 0.75	| 0.01	| 0.08  |
| kio-zip	 | n/a	 | n/a	 | n/a   |
| kio-krarc	| 0.53	| 0.06	| 0.03  |
| mc-uzip	 | 0.38	| 0.36	| 0.02  |
| gvfs-fuse	| n/a	 | n/a	 | n/a   |
| unpackfs	| n/a	 | n/a	 | n/a   |
| avfs-fuse	| n/a	 | n/a	 | n/a   |
| fusej-zip	| n/a	 | n/a	 | n/a   |

## Add big file to small archive (add-big-small)
| Program | real | user | system |
|:--------|:-----|:-----|:-------|
| fuse-zip	| 0.05	| 0.00	| 0.02  |
| kio-zip	 | n/a	 | n/a	 | n/a   |
| kio-krarc	| 0.13	| 0.05	| 0.01  |
| mc-uzip	 | 0.09	| 0.07	| 0.02  |
| gvfs-fuse	| n/a	 | n/a	 | n/a   |
| unpackfs	| n/a	 | n/a	 | n/a   |
| avfs-fuse	| n/a	 | n/a	 | n/a   |
| fusej-zip	| n/a	 | n/a	 | n/a   |

## Add big file to big archive (add-big-big)
| Program | real | user | system |
|:--------|:-----|:-----|:-------|
| fuse-zip	| 0.66	| 0.00	| 0.06  |
| kio-zip	 | n/a	 | n/a	 | n/a   |
| kio-krarc	| 0.53	| 0.07	| 0.02  |
| mc-uzip	 | 0.38	| 0.36	| 0.01  |
| gvfs-fuse	| n/a	 | n/a	 | n/a   |
| unpackfs	| n/a	 | n/a	 | n/a   |
| avfs-fuse	| n/a	 | n/a	 | n/a   |
| fusej-zip	| n/a	 | n/a	 | n/a   |

## Compress (a part of) linux kernel sources (zip-linuxsrc)
| Program | real | user | system |
|:--------|:-----|:-----|:-------|
| fuse-zip	| 25.58	| 0.33	| 1.06  |
| kio-zip	 | n/a	  | n/a	 | n/a   |
| kio-krarc	| 2005.02	| 5.91	| 1.04  |
| mc-uzip	 | 572.13	| 446.28	| 103.49  |
| gvfs-fuse	| n/a	  | n/a	 | n/a   |
| unpackfs	| n/a	  | n/a	 | n/a   |
| avfs-fuse	| n/a	  | n/a	 | n/a   |
| fusej-zip	| n/a	  | n/a	 | n/a   |

## Uncompress (a part of) linux kernel sources (unzip-linuxsrc)
| Program | real | user | system |
|:--------|:-----|:-----|:-------|
| fuse-zip	| 2.93	| 0.12	| 0.79  |
| kio-zip	 | 12.63	| 4.42	| 0.80  |
| kio-krarc	| 47.40	| 2.53	| 0.26  |
| mc-uzip	 | 2353.59	| 1870.04	| 381.76  |
| gvfs-fuse	| FAIL	| FAIL	| FAIL  |
| unpackfs	| 28.18	| 0.11	| 0.78  |
| avfs-fuse	| 4.09	| 0.10	| 0.92  |
| fusej-zip	| FAIL	| FAIL	| FAIL  |


---

Legend:

  * **n/a** -- functionality not supported
  * **FAIL** -- program crashed or freezed
