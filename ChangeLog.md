﻿**2019-09-08** Alexander Galanin

    * Released 0.7.0:
      1. PKWARE NTFS extra field support. Support high-precision timestamps via
         this extension.
      1. PKWARE NTFS extra field: preserve 'reserved' field and unknown tags.
      1. Full read support for PKWARE UNIX extra field features. Write support
         for all PKWARE UNIX features except hard links.
      1. Full support for block/character devices, sockets and FIFOs.
      1. Read only support for hard links.
      1. Support for file and archive comments.
      1. Work-around for PKWARE's interpretation of 'version made by' field.
      1. Support FUSE 2.9.
      1. Update ctime on rename.
      1. Now requires C++11-compilant compiler to build.

**2019-08-03** Alexander Galanin

  * Released 0.6.2:
    1. Fixed parsing of Info-Zip New Unix extra field if size of UID or GID is
       less than size of uid_t or gid_t.

**2019-07-17** Alexander Galanin

  * Released 0.6.1:
    1. Fixed extra field timestamps parsing on 64-bit systems.
    1. Fixed compilation with gcc 8.3.0 (patch from Michael Davis), closes issue #55.

**2019-03-12** Alexander Galanin

  * Released 0.6.0:
    1. Fixed incompatibility with iconv module.
    1. inode numbers is now unique.
    1. Include libzip version on --version output.
    1. Set minimum libzip version to 1.0, remove deprecated functions usage.
    1. Fixed extended set of compiler warnings (type conversion, old style
       casts).
    1. Added debug utils.

**2018-12-01** Alexander Galanin

  * Released 0.5.0:
    1. Issue #54: Force open in read-only mode if archive file or its parent
       directory is not writable.

**2018-02-04** Alexander Galanin

  * Released 0.4.5:
    1. Issue #52: Print error message and exit on nonexistent file in read-only mode.
    1. Issue #51: Check return value of chdir().
    1. Add Reiner Herrmann's patch to make the build reproducible.
    1. Fixed errors found by static code analysis.

**2017-12-04** Alexander Galanin

  * Released 0.4.4:
    1. Fixed problem with README installation.

**2017-12-03** Alexander Galanin

  * Released 0.4.3:
    1. License changed to GPLv3 or later because LGPLv3 is too confusing.
    1. Support mknod() system call.
    1. Fixed out of bounds write on sparse file (issue #50).
    1. Fixed timestamp and file attribute fields saving into archive.

**2016-02-28** Alexander Galanin

  * Released 0.4.2:
    1. Properly handle ZIP\_SOURCE\_SUPPORTS call introduced in libzip 1.0 (fixes #46)

**2014-07-04** Alexander Galanin

  * Released 0.4.1:
    1. Fixed problem with subdir module support.
    1. Applied makefile conventions from GNU Coding Standards.

**2014-01-14** Alexander Galanin

  * Released 0.4.0:
    1. Symbolic links (issue #8)
    1. UNIX file permissions and owner info (issue #36)
    1. DOS file attributes
    1. Creation/access time

**2013-12-10** Alexander Galanin

  * Released 0.3.2:
    1. Fixed compilation problem on Mac OS X

**2013-12-07** Alexander Galanin

  * Released 0.3.1:
    1. Fixed compilation problems
    1. Fixed memory reservation

**2013-06-29** Alexander Galanin

  * Released 0.3.0:
    1. ZIP64 support (issue #30, issue #34, issue #35)
    1. Relative and absolute paths support (issue #23)
    1. Fixed compilation problems (issue #29, issue #31)

**2010-12-06** Alexander Galanin

  * Released 0.2.13:
    1. Fixed issue #27: Android APK Support
    1. Fixed issue #28: doesn't honor the -r flag for some zip files

**2010-02-07** Alexander Galanin

  * Released 0.2.12:
    1. Fixed problem with lost new file after truncate().
    1. Fixed problems with rename(): Fixed various rename() problems: lost path for subdirectory entries, duplicates of moved directories in a hierarchy, invalid key in map after rename.
    1. Fixed unitialized values in read() call.
    1. Fixed memory leaks: buffer allocated for file content not freeed in NEW state, incorrect buffer size in truncate().
    1. Fixed non-fatal memory leaks: FUSE options not freeed after use, memory leak in help/version mode, internal data structures not freeed if FUSE setup failed.
    1. More correct corrupted files handling.
    1. More correct memory insufficiency errors handling.

**2010-01-26** Alexander Galanin

  * Released 0.2.11:
    1. Fixed issue #25: does not compile with libfuse <= 2.8

**2010-01-09** Alexander Galanin

  * Released 0.2.10:
    1. Fixed issue #14: added '-r' option description.
    1. Added note about converting file names inside archive (for Russians who uses 'another OS')

**2010-01-08** Alexander Galanin

  * Released 0.2.9:
    1. Fixed issue #22, now command-line options are correctly processed

**2009-11-18** Alexander Galanin

  * Released 0.2.8:
    1. Fixed issue #20: incorrect directory size reported

**2008-12-06** Alexander Galanin

  * Released 0.2.7:
    1. Fixed segfault if user tried to re-open corrupted file from an invalid archive.

**2008-09-29** Alexander Galanin

  * Released 0.2.6:
    1. Fixed: Compilation error on FreeBSD/Mac OS X with FUSE 2.7

**2008-08-24** Alexander Galanin

  * Released 0.2.5:
    1. Fixed: Archives containing files whose parent directory does not present in archive, reported as broken.

**2008-08-09** Alexander Galanin

  * Released 0.2.4:
    1. fixed wrong directory size on 32-bit Linux machines

**2008-06-26** Alexander Galanin

  * Released 0.2.3:
    1. fixed problem with time and size of new files
    1. free space on file system now equal to free space in archive dir
    1. added missing includes
    1. removed GNU-specific commands in Makefile

**2008-06-16** Alexander Galanin

  * Released 0.2.2:
    1. re-licensed under LGPLv3+
    1. fixed problem with file modification time
    1. fixed problems with compilation on non-Linux systems

**2008-06-14** Alexander Galanin

  * Released 0.2.1:
    1. fixed bug with file size on 32-bit systems
    1. fixed compilation problems on non-GNU platforms
    1. fixed compilation problems on GCC 4.3+
    1. added stubs for `*`xattr() system calls
    1. more checks for error cases

**2008-06-11** Alexander Galanin

  * Released 0.2.0:
    1. implemented all functions to mount archive read-write
    1. big internal refactoring
    1. added performance tests
    1. fixed problem with simultaneous file access
    1. fixed problem with one-character file names
  * Known limitations:
    1. archives with items like '../../somefile' recognited as broken

**2008-06-04** Alexander Galanin

  * Initial release 0.1.0:
    1. implemented all base functions for read-only archive support
